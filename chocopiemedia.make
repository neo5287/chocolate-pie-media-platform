; ----------------
; Chocolate Pie Media Platform Makefile 
; Permanent URL: https://bitbucket.org/cpmadmin/chocolate-pie-media-platform.git
; Build Version: v0.1
; Build Name: build_0_1
; Build Description: Initial Repack of the demo install
; Filename: chocopiemedia.make
; ----------------  

; ------------  
; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.
  
core = 7.x
api = 2

; ------------      
; Modules
; ------------

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.3"
projects[ctools][type] = "module"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"
projects[pathauto][type] = "module"


projects[robotstxt][version] = "1.1"
projects[robotstxt][subdir] = "contrib"
projects[robotstxt][type] = "module"

projects[token][version] = "1.5"
projects[token][subdir] = "contrib"
projects[token][type] = "module"

projects[entitycache][version] = "1.x-dev"
projects[entitycache][subdir] = "contrib"
projects[entitycache][type] = "module"

projects[views][version] = "3.7"
projects[views][subdir] = "contrib"
projects[views][type] = "module"

projects[devel][version] = "1.3"
projects[devel][subdir] = "contrib"
projects[devel][type] = "module"

projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "1.4"
projects[google_analytics][type] = "module"

projects[xmlsitemap][subdir] = "contrib"
projects[xmlsitemap][version] = "2.0-rc2"
projects[xmlsitemap][type] = "module"

projects[metatag][subdir] = "contrib"
projects[metatag][version] = "1.0-beta7"
projects[metatag][type] = "module"

projects[globalredirect][subdir] = "contrib"
projects[globalredirect][version] = "1.5"
projects[globalredirect][type] = "module"

projects[redirect][subdir] = "contrib"
projects[redirect][version] = "1.0-rc1"
projects[redirect][type] = "module"

projects[page_title][subdir] = "contrib"
projects[page_title][version] = "2.7"
projects[page_title][type] = "module"

projects[seo_checklist][subdir] = "contrib"
projects[seo_checklist][version] = "4.1"
projects[seo_checklist][type] = "module"

projects[seo_checker][subdir] = "contrib"
projects[seo_checker][version] = "1.6"
projects[seo_checker][type] = "module"

projects[search404][subdir] = "contrib"
projects[search404][version] = "1.3"
projects[search404][type] = "module"

projects[checklistapi][subdir] = "contrib"
projects[checklistapi][version] = "1.1"
projects[checklistapi][type] = "module"

projects[ckeditor][subdir] = "contrib"
projects[ckeditor][version] = "1.13"
projects[ckeditor][type] = "module"

projects[site_verify][subdir] = "contrib"
projects[site_verify][version] = "1.0"
projects[site_verify][type] = "module"

projects[wellcontact][subdir] = "contrib"
projects[wellcontact][type] = "module"
projects[wellcontact][download][type] = "git"
projects[wellcontact][download][url] = "https://bitbucket.org/cpmadmin/chocolate-pie-media-wellfolio-contact.git"

; ------------
; Libraries
; ------------

; ------------
; ckeditor
; ------------
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"
libraries[ckeditor][destination] = "libraries"
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.tar.gz"

